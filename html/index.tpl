<html>
<head><title>Esp8266 web server</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="style.min.css">
</head>
<body>
<div class="row">
<div class="small-12 medium-6 small-centered columns content">
<h1>Hackitt & Bodgitt Designs 2016</h1>
<p>
ESP8266 MQTT device Configuration<br> Aidan Ruff (aidan@ruffs.org) and Peter Scargill (peter@scargill.org)<br>
(Aka H & B Designs)
<ul>
<li>Setup the WiFi access and MQTT Server details <a href="/wifi"> here</a><br></li>
<li>Control the <a href="led.tpl">Relay and LED</a><br></li>
<li>You can download the raw binary image of the ESP module <a href="flash.bin">Here</a></li>
</ul>
</p>
</p>
</div>
</div>
</body></html>
